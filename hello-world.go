package main

import "fmt"

func main() {
	var name string
	fmt.Print("Please Enter your name : ")
	fmt.Scan(&name)
	fmt.Printf("Hello %v This is GO", name)
	fmt.Print("\nThank you for Learning Go")
}
